SHELL=/bin/bash

STACK=acceptance-tests

deploy:
	docker stack deploy --resolve-image always \
						--with-registry-auth \
						-c docker-compose.infrastructure.yaml \
						-c docker-compose.services.yaml \
 						-c docker-compose.selenium.yaml $(STACK)

ls:
	docker stack services $(STACK)

rm:
	docker stack rm $(STACK)

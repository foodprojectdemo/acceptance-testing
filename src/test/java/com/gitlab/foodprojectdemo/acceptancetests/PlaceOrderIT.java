package com.gitlab.foodprojectdemo.acceptancetests;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import com.gitlab.foodprojectdemo.acceptancetests.pages.cart.Cart;
import com.gitlab.foodprojectdemo.acceptancetests.pages.cart.CartPage;
import com.gitlab.foodprojectdemo.acceptancetests.pages.checkout.CheckoutPage.CheckoutForm;
import com.gitlab.foodprojectdemo.acceptancetests.pages.orders.Order;
import com.gitlab.foodprojectdemo.acceptancetests.pages.orders.OrdersPage;
import com.gitlab.foodprojectdemo.acceptancetests.pages.restaurants.RestaurantsPage;

import java.net.URL;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static com.gitlab.foodprojectdemo.acceptancetests.Faker.faker;

@Slf4j
@SpringBootTest
class PlaceOrderIT extends AbstractWebDriverTest {

    @Value("${homepage.uri}")
    private URL homePage;

    @Value("${cart-page.uri}")
    private URL cartUrl;

    @Value("${orders-page.uri}")
    private URL ordersPageUri;

    private Cart cart;

    private Cart addDishToCart() {
        var restaurantsPage = new RestaurantsPage(webDriver, homePage);

        var restaurant = restaurantsPage.getAnyRestaurant();
        log.info("selected {}", restaurant);

        var restaurantPage = restaurant.restaurantPage();

        var dish = restaurantPage.getAnyDish();

        log.info("selected {}", dish);

        restaurantPage.addDishToCart(dish);

        var cartPage = new CartPage(webDriver, cartUrl);

        var cart = cartPage.getCart();

        log.info("{}", cart);

        assertThat(cart.isNotEmpty()).isTrue();
        assertThat(cart.contains(dish)).isTrue();

        return cart;
    }

    @BeforeEach
    void setUp() {
        cart = addDishToCart();
    }


    @Test
    void shouldGoToCheckout() {
        var cartPage = new CartPage(webDriver, cartUrl);

        var checkoutPage = cartPage.goToCheckout();

        var form = CheckoutForm.builder()
                .name(faker().name().fullName())
                .phone(faker().phoneNumber().phoneNumber())
                .email(faker().internet().emailAddress())
                .street(faker().address().streetAddress())
                .house(faker().number().digits(2))
                .floor(faker().number().digits(2))
                .flat(faker().number().digits(3))
                .build();

        var orderId = checkoutPage.checkout(form);

        log.info("checkout new order with ID {}", orderId);

        var ordersPage = new OrdersPage(webDriver, ordersPageUri);

        var orders = ordersPage.orders();
        log.info("{}", orders);

        assertThat(orders).anyMatch(order -> order.getOrderId().equals(orderId));
    }


}

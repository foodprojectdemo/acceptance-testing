package com.gitlab.foodprojectdemo.acceptancetests;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import com.gitlab.foodprojectdemo.acceptancetests.pages.cart.Cart;
import com.gitlab.foodprojectdemo.acceptancetests.pages.cart.CartPage;
import com.gitlab.foodprojectdemo.acceptancetests.pages.restaurants.RestaurantsPage;

import java.net.URL;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@SpringBootTest
class AddDishToCartIT extends AbstractWebDriverTest {

    @Value("${homepage.uri}")
    private URL homePage;

    @Value("${cart-page.uri}")
    private URL cartUrl;

    @Test
    void shouldAddDishToCart() {
        var restaurantsPage = new RestaurantsPage(webDriver, homePage);

        var restaurant = restaurantsPage.getAnyRestaurant();
        log.info("selected {}", restaurant);

        var restaurantPage = restaurant.restaurantPage();

        var dish = restaurantPage.getAnyDish();

        log.info("selected {}", dish);

        restaurantPage.addDishToCart(dish);

        var cartPage = new CartPage(webDriver, cartUrl);

        var cart = cartPage.getCart();

        log.info("{}", cart);

        assertThat(cart.isNotEmpty()).isTrue();
        assertThat(cart.contains(dish)).isTrue();
    }

}

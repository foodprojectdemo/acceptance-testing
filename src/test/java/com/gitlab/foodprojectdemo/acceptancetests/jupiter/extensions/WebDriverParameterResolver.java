package com.gitlab.foodprojectdemo.acceptancetests.jupiter.extensions;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.ParameterContext;
import org.junit.jupiter.api.extension.ParameterResolutionException;
import org.junit.jupiter.api.extension.support.TypeBasedParameterResolver;
import org.openqa.selenium.WebDriver;
import org.springframework.test.context.junit.jupiter.SpringExtension;

public class WebDriverParameterResolver extends TypeBasedParameterResolver<WebDriver> {

    @Override
    public WebDriver resolveParameter(ParameterContext parameterContext, ExtensionContext extensionContext) throws ParameterResolutionException {
        var applicationContext = SpringExtension.getApplicationContext(extensionContext);
        return applicationContext.getBean(WebDriver.class);
    }
}

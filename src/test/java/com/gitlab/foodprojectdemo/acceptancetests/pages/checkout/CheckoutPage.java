package com.gitlab.foodprojectdemo.acceptancetests.pages.checkout;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.SneakyThrows;
import lombok.Value;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.gitlab.foodprojectdemo.acceptancetests.pages.Page;

import java.net.URL;

@Slf4j
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CheckoutPage extends Page {
    @Value
    @Builder
    public static class CheckoutForm {
        String name;
        String phone;
        String email;
        String street;
        String house;
        String floor;
        String flat;
    }

    @FindBy(css = "form [name=name]")
    WebElement nameInput;

    @FindBy(css = "form [name=phone]")
    WebElement phoneInput;

    @FindBy(css = "form [name=email]")
    WebElement emailInput;

    @FindBy(css = "form [name=street]")
    WebElement streetInput;

    @FindBy(css = "form [name=house]")
    WebElement houseInput;

    @FindBy(css = "form [name=floor]")
    WebElement floorInput;

    @FindBy(css = "form [name=flat]")
    WebElement flatInput;

    @FindBy(css = "form button[type=submit]")
    WebElement submit;

    final By SuccessTitleSelector = By.tagName("h2");

    final By orderIdSelector = By.cssSelector("h3 span");


    public CheckoutPage(WebDriver webDriver, URL pageUrl) {
        super(webDriver, pageUrl);
    }

    @SneakyThrows
    public String checkout(CheckoutForm form) {
        checkLocationAndGoToPage();
        PageFactory.initElements(webDriver, this);

        nameInput.clear();
        nameInput.sendKeys(form.name);
        phoneInput.clear();
        phoneInput.sendKeys(form.phone);
        emailInput.clear();
        emailInput.sendKeys(form.email);
        streetInput.clear();
        streetInput.sendKeys(form.street);
        houseInput.clear();
        houseInput.sendKeys(form.house);
        floorInput.clear();
        floorInput.sendKeys(form.floor);
        flatInput.clear();
        flatInput.sendKeys(form.flat);

        submit.click();

//        new WebDriverWait(webDriver, 1).until(driver -> driver.findElement(SuccessTitle));

        if (!webDriver.findElement(SuccessTitleSelector).getText().equalsIgnoreCase("Success")) {
            log.error("unsuccessful checkout");
        }

        return webDriver.findElement(orderIdSelector).getText();
    }
}

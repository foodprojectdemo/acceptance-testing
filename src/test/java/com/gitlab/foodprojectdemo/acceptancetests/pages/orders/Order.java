package com.gitlab.foodprojectdemo.acceptancetests.pages.orders;

import lombok.Value;

@Value
public class Order {
    String orderId;
    String status;
}

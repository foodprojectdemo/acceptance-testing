package com.gitlab.foodprojectdemo.acceptancetests.pages;

import lombok.AllArgsConstructor;
import org.openqa.selenium.WebDriver;

import java.net.URL;

@AllArgsConstructor
public abstract class Page {
    protected final WebDriver webDriver;
    public final URL pageUrl;

    public void goToPage() {
        webDriver.navigate().to(pageUrl);
    }

    protected void checkLocationAndGoToPage() {
        if (!webDriver.getCurrentUrl().equals(pageUrl.toString())) {
            goToPage();
        }
    }
}

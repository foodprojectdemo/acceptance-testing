package com.gitlab.foodprojectdemo.acceptancetests.pages.restaurant;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.gitlab.foodprojectdemo.acceptancetests.pages.Page;

import java.net.URL;
import java.util.Random;

public class RestaurantPage extends Page {

    private final By dishSelector = By.cssSelector(".dish");
    private final By dishIdSelector = By.cssSelector("input[name=dishId]");
    private final By dishNameSelector = By.cssSelector(".dish-name");
    private final By dishPriceSelector = By.cssSelector(".price");

    public RestaurantPage(WebDriver webDriver, URL pageUrl) {
        super(webDriver, pageUrl);
    }

    public Dish getAnyDish() {
        checkLocationAndGoToPage();

        var dishElements = webDriver.findElements(dishSelector);
        var dishElement = dishElements.get(new Random().nextInt(dishElements.size()));

        var id = dishElement.findElement(dishIdSelector).getAttribute("value");
        var name = dishElement.findElement(dishNameSelector).getText();
        var price = Integer.parseInt(dishElement.findElement(dishPriceSelector).getText().replaceAll("[^0-9]+",""));

        return new Dish(id, name, price);
    }

    public void addDishToCart(Dish dish) {
        checkLocationAndGoToPage();

        var addToCartBtnSelector = By.cssSelector(String.format(".add-to-cart-form input[name=dishId][value=\"%s\"] + input[type=submit]", dish.getDishId()));
        var addToCartBtn = webDriver.findElement(addToCartBtnSelector);
        addToCartBtn.click();

        new WebDriverWait(webDriver, 3).until(driver ->
                driver.findElement(By.cssSelector(".toast-body")).getText().equalsIgnoreCase(String.format("%s added to your cart", dish.getName()))
        );
    }


}

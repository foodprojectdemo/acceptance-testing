package com.gitlab.foodprojectdemo.acceptancetests.pages.cart;

import lombok.AccessLevel;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import com.gitlab.foodprojectdemo.acceptancetests.pages.Page;
import com.gitlab.foodprojectdemo.acceptancetests.pages.checkout.CheckoutPage;

import java.net.URL;
import java.util.stream.Collectors;

@Slf4j
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class CartPage extends Page {

    By cartRowSelector = By.cssSelector(".card .row");
    By cartRowNameSelector = By.tagName("h5");
    By cartRowQuantitySelector = By.cssSelector("input[name=quantity]");
    By cartRowPriceSelector = By.cssSelector(".item-price");
    By cartRowTotalSelector = By.cssSelector(".item-total");
    By checkoutBtnSelector = By.cssSelector("#checkout-form button");

    public CartPage(WebDriver webDriver, @Value("${cart-page.uri}") URL pageUrl) {
        super(webDriver, pageUrl);
    }


    public Cart getCart() {
        checkLocationAndGoToPage();

        var items = webDriver.findElements(cartRowSelector).stream().map(this::cartRowToCartItem).collect(Collectors.toList());

        return new Cart(items);
    }

    @SneakyThrows
    public CheckoutPage goToCheckout() {
        checkLocationAndGoToPage();

        webDriver.findElement(checkoutBtnSelector).click();
        return new CheckoutPage(webDriver, new URL(webDriver.getCurrentUrl()));
    }


    private CartItem cartRowToCartItem(WebElement cartRow) {
        var name = cartRow.findElement(cartRowNameSelector).getText();
        var quantity = Integer.parseInt(cartRow.findElement(cartRowQuantitySelector).getAttribute("value"));
        var price = parsePrice(cartRow.findElement(cartRowPriceSelector).getText());
        var total = parsePrice(cartRow.findElement(cartRowTotalSelector).getText());
        return new CartItem(name, price, quantity, total);
    }

    private int parsePrice(String price) {
        return Integer.parseInt(price.replaceAll("[^0-9]+", ""));
    }
}

package com.gitlab.foodprojectdemo.acceptancetests.pages.cart;

import lombok.Value;
import com.gitlab.foodprojectdemo.acceptancetests.pages.restaurant.Dish;

import java.net.URL;
import java.util.Collection;

@Value
public class Cart {
    Collection<CartItem> items;

    public boolean isEmpty() {
        return items.isEmpty();
    }

    public boolean isNotEmpty() {
        return !items.isEmpty();
    }

    public boolean contains(Dish dish) {
        return items.stream().anyMatch(cartItem -> cartItem.getName().equals(dish.getName()));
    }
}

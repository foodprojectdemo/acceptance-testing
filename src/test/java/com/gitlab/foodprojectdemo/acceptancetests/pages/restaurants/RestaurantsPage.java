package com.gitlab.foodprojectdemo.acceptancetests.pages.restaurants;

import lombok.AccessLevel;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.gitlab.foodprojectdemo.acceptancetests.pages.Page;

import java.net.URL;
import java.util.Random;

@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class RestaurantsPage extends Page {

    By restaurantSelector = By.cssSelector(".restaurant");
    By restaurantNameSelector = By.cssSelector(".restaurant-name");
    By restaurantLinkSelector = By.tagName("a");

    public RestaurantsPage(WebDriver webDriver, URL pageUrl) {
        super(webDriver, pageUrl);
    }


    @SneakyThrows
    public Restaurant getAnyRestaurant() {
        checkLocationAndGoToPage();

        var elements = webDriver.findElements(restaurantSelector);
        assert elements.size() > 0 : "Not ready restaurants: " + webDriver.getPageSource();
        var element = elements.get(new Random().nextInt(elements.size()));
        var name = element.findElement(restaurantNameSelector).getText();
        var url = new URL(element.findElement(restaurantLinkSelector).getAttribute("href"));

        return new Restaurant(name, url, webDriver);
    }
}

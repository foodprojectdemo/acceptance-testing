package com.gitlab.foodprojectdemo.acceptancetests.pages.orders;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.gitlab.foodprojectdemo.acceptancetests.pages.Page;

import java.net.URL;
import java.util.List;
import java.util.stream.Collectors;
@Slf4j
public class OrdersPage extends Page {
    public OrdersPage(WebDriver webDriver, URL pageUrl) {
        super(webDriver, pageUrl);
    }

    public List<Order> orders() {
        checkLocationAndGoToPage();

        var orderElements = webDriver.findElements(By.cssSelector(".container div.col-sm-12 > div"));

        return orderElements.stream().map(orderElement -> {
            var orderId = orderElement.findElement(By.tagName("h4")).getText();
            var status = orderElement.findElement(By.cssSelector("h4 + div")).getText();
            return new Order(orderId, status);
        }).collect(Collectors.toList());
    }
}

package com.gitlab.foodprojectdemo.acceptancetests.pages.restaurants;

import lombok.ToString;
import lombok.Value;
import org.openqa.selenium.WebDriver;
import com.gitlab.foodprojectdemo.acceptancetests.pages.restaurant.RestaurantPage;

import java.net.URL;

@Value
@ToString(exclude = "webDriver")
public class Restaurant {
    String name;
    URL url;
    WebDriver webDriver;


    public RestaurantPage restaurantPage() {
        return new RestaurantPage(webDriver, url);
    }


}

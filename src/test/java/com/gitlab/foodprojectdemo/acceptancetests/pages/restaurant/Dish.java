package com.gitlab.foodprojectdemo.acceptancetests.pages.restaurant;

import lombok.Value;

import java.net.URL;

@Value
public class Dish {
    String dishId;
    String name;
    int price;
}

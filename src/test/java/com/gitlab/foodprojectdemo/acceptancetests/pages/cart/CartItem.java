package com.gitlab.foodprojectdemo.acceptancetests.pages.cart;

import lombok.Value;

@Value
public class CartItem {
//    String dishId;
    String name;
    int price;
    int quantity;
    int total;
}

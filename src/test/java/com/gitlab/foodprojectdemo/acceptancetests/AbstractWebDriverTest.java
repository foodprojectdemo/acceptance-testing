package com.gitlab.foodprojectdemo.acceptancetests;

import lombok.Getter;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import com.gitlab.foodprojectdemo.acceptancetests.jupiter.extensions.WebDriverParameterResolver;

@ExtendWith({
        WebDriverParameterResolver.class,
})
public abstract class AbstractWebDriverTest {

    @Getter
    protected WebDriver webDriver;

    @BeforeEach
    private void setUp(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    @AfterEach
    private void tearDown() {
        if (webDriver != null)
            webDriver.quit();
    }
}

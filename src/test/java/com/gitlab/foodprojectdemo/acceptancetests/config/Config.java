package com.gitlab.foodprojectdemo.acceptancetests.config;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;

import java.net.URL;

import static org.springframework.beans.factory.config.ConfigurableBeanFactory.SCOPE_PROTOTYPE;

@Slf4j
@Configuration
class Config {

    @Value("${webdriver.uri}")
    private URL remoteAddress;

    @Bean
    @Scope(SCOPE_PROTOTYPE)
    public WebDriver webDriver() {
        log.info("get webDriver: {}", remoteAddress);
        var options = new FirefoxOptions();
        options.addPreference("permissions.default.image", 2);
        return new RemoteWebDriver(remoteAddress, options);
    }

}
